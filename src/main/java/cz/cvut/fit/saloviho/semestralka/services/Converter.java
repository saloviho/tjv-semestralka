package cz.cvut.fit.saloviho.semestralka.services;

import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import cz.cvut.fit.saloviho.semestralka.rest.MarkController;
import cz.cvut.fit.saloviho.semestralka.rest.StudentController;
import cz.cvut.fit.saloviho.semestralka.rest.SubjectController;
import cz.cvut.fit.saloviho.semestralka.rest.dto.MarkDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.StudentDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.SubjectDto;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class Converter {
    public static final RepresentationModelAssemblerSupport<StudentEntity, StudentDto> STUDENT_TO_DTO = new RepresentationModelAssemblerSupport<StudentEntity, StudentDto>(StudentController.class, StudentDto.class) {
        @Override
        public StudentDto toModel(StudentEntity e) {
            StudentDto p = new StudentDto(e.getId(), e.getFirstname(), e.getLastname());
            p.add(linkTo(methodOn(StudentController.class).getById(e.getId())).withSelfRel());
            return p;
        }
    };

    public static final RepresentationModelAssemblerSupport<SubjectEntity, SubjectDto> SUBJECT_TO_DTO
            = new RepresentationModelAssemblerSupport<SubjectEntity, SubjectDto>(SubjectController.class, SubjectDto.class) {
        @Override
        public SubjectDto toModel(SubjectEntity e) {
            SubjectDto p = new SubjectDto(e.getId(), e.getShortname(), e.getFullname());
            p.add(linkTo(methodOn(SubjectController.class).getById(e.getId())).withSelfRel());
            return p;
        }
    };

    public static final RepresentationModelAssemblerSupport<MarkEntity, MarkDto> MARK_TO_DTO
            = new RepresentationModelAssemblerSupport<MarkEntity, MarkDto>(MarkController.class, MarkDto.class) {
        @Override
        public MarkDto toModel(MarkEntity e) {
            MarkDto p = new MarkDto(e.getId(), e.getMark());
            p.add(linkTo(methodOn(MarkController.class).getById(e.getId())).withSelfRel());
            return p;
        }
    };

    public static final StudentEntity DTO_TO_STUDENT(StudentDto dto)
    {
        return new StudentEntity(dto.getId(), dto.getFirstname(), dto.getLastname());
    }

    public static final SubjectEntity DTO_TO_SUBJECT(SubjectDto dto)
    {
        return new SubjectEntity(dto.getId(), dto.getShortname(), dto.getFullname());
    }

    public static final MarkEntity DTO_TO_MARK(MarkDto dto)
    {
        return new MarkEntity(dto.getId(), dto.getMark());
    }
}
