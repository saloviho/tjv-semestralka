package cz.cvut.fit.saloviho.semestralka.data.dao;

import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {
    Optional<StudentEntity> findByFirstnameAndLastname(String firstName, String lastName);
    Collection<SubjectEntity> findAllSubjectsByFirstnameAndLastname(String firstName, String lastName);
}
