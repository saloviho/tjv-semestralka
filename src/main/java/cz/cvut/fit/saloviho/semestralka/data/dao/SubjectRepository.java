package cz.cvut.fit.saloviho.semestralka.data.dao;

import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, Integer> {
    Optional<SubjectEntity> findByShortname(String shortName);
    Collection<StudentEntity> findAllAppliedStudentsByShortname(String shortName);
}