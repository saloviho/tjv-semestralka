package cz.cvut.fit.saloviho.semestralka.data.model;

import cz.cvut.fit.saloviho.semestralka.rest.StudentController;
import cz.cvut.fit.saloviho.semestralka.rest.dto.StudentDto;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import javax.persistence.*;
import java.util.Collection;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Entity
@Table(name = "students")
public class StudentEntity {
    @Id
    @GeneratedValue
    private Integer id;
    private String firstname;
    private String lastname;

    @ManyToMany(cascade = {CascadeType.ALL})
    Collection<SubjectEntity> subjects;

    @OneToMany(cascade = {CascadeType.ALL})
    Collection<MarkEntity> marks;

    public StudentEntity() {
    }

    public StudentEntity(Integer id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public StudentEntity(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Collection<SubjectEntity> getSubjects() {
        return subjects;
    }

    public void setSubjects(Collection<SubjectEntity> subjects) {
        this.subjects = subjects;
    }

    public Collection<MarkEntity> getMarks() {
        return marks;
    }

    public void setMarks(Collection<MarkEntity> marks) {
        this.marks = marks;
    }

    public void addSubject(SubjectEntity subject) { this.subjects.add(subject); }

    public void removeSubject(SubjectEntity subject) { this.subjects.removeIf(e -> e.equals(subject)); }
}
