package cz.cvut.fit.saloviho.semestralka.rest;

import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import cz.cvut.fit.saloviho.semestralka.rest.dto.MarkDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.StudentDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.SubjectDto;
import cz.cvut.fit.saloviho.semestralka.services.Converter;
import cz.cvut.fit.saloviho.semestralka.services.MarkService;
import cz.cvut.fit.saloviho.semestralka.services.StudentService;
import cz.cvut.fit.saloviho.semestralka.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(SubjectDto.class)
@RequestMapping(value = "/api/subjects")
public class SubjectController {
    @Autowired
    StudentService studentService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    MarkService markService;

    @Autowired
    private EntityLinks entityLinks;

    @GetMapping
    public HttpEntity<Collection<SubjectDto>> getAll()
    {
        Link linkToList = entityLinks.linkToCollectionResource(SubjectDto.class).withRel("list");
        Collection<SubjectDto> subjectsDto = Converter.SUBJECT_TO_DTO.toCollectionModel(subjectService.readAll()).getContent();
        return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(subjectsDto);
    }

    @PostMapping
    public HttpEntity<SubjectDto> create(@RequestBody SubjectDto dto)
    {
        SubjectEntity e = subjectService.createOrUpdate(new SubjectEntity(dto.getShortname(), dto.getFullname()));
        SubjectDto result = Converter.SUBJECT_TO_DTO.toModel(e);
        return ResponseEntity.created(entityLinks.linkForItemResource(SubjectDto.class, result.getId()).toUri()).body(result);
    }

    @PutMapping
    public HttpEntity<SubjectDto> updateOrCreate(@RequestBody SubjectDto dto)
    {
        Link linkToList = entityLinks.linkToCollectionResource(SubjectDto.class).withRel("list");
        SubjectEntity e = subjectService.createOrUpdate(Converter.DTO_TO_SUBJECT(dto));
        SubjectDto result = Converter.SUBJECT_TO_DTO.toModel(e);
        return ResponseEntity.ok().header(HttpHeaders.LINK, result.getLink("self").toString())
                .header(HttpHeaders.LINK, linkToList.toString()).body(result);
    }


    @GetMapping("/{id}")
    public HttpEntity<SubjectDto> getById(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(SubjectDto.class).withRel("list");
        Optional<SubjectEntity> res = subjectService.readById(id);
        if(res.isPresent()) {
            SubjectDto dto = Converter.SUBJECT_TO_DTO.toModel(res.get());
            dto.add(linkToList);
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString())
                    .header(HttpHeaders.LINK, dto.getLink("self").toString()).body(dto);
        }
        else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(SubjectDto.class).withRel("list");
        Optional<SubjectEntity> res = subjectService.readById(id);
        if(res.isPresent()) {
            subjectService.deleteById(res.get().getId());
            return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
        }
        else return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/students")
    public HttpEntity<Collection<StudentDto>> getStudents(@PathVariable Integer id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getStudents(id)).withRel("list");
        Optional<SubjectEntity> subject = subjectService.readById(id);
        if(subject.isPresent())
        {
            Collection<StudentEntity> students = subject.get().getAppliedStudents();
            Collection<StudentDto> studentsDto = Converter.STUDENT_TO_DTO.toCollectionModel(students).getContent();
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(studentsDto);
        }else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @PutMapping("/{id}/students/{student_id}")
    public HttpEntity addStudent(@PathVariable Integer id, @PathVariable Integer student_id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getStudents(id)).withRel("list");
        subjectService.addStudent(id, student_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}/students/{student_id}")
    public HttpEntity removeStudent(@PathVariable Integer id, @PathVariable Integer student_id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getStudents(id)).withRel("list");
        subjectService.removeStudent(id, student_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @GetMapping("/{id}/marks")
    public HttpEntity<Collection<MarkDto>> getMarks(@PathVariable Integer id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getMarks(id)).withRel("list");
        Optional<SubjectEntity> subject = subjectService.readById(id);
        if(subject.isPresent())
        {
            Collection<MarkEntity> marks = subject.get().getMarks();
            Collection<MarkDto> marksDto = Converter.MARK_TO_DTO.toCollectionModel(marks).getContent();
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(marksDto);
        }else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @PutMapping("/{id}/marks/{mark_id}")
    public HttpEntity addMark(@PathVariable Integer id, @PathVariable Integer mark_id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getMarks(id)).withRel("list");
        subjectService.addMark(id, mark_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}/marks/{mark_id}")
    public HttpEntity removeMark(@PathVariable Integer id, @PathVariable Integer mark_id)
    {
        Link linkToList = linkTo(methodOn(SubjectController.class).getMarks(id)).withRel("list");
        subjectService.removeMark(id, mark_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }
}
