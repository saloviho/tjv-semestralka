package cz.cvut.fit.saloviho.semestralka.data.dao;

import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkRepository extends JpaRepository<MarkEntity, Integer> {
}
