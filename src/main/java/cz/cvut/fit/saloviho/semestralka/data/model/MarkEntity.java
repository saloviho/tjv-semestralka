package cz.cvut.fit.saloviho.semestralka.data.model;

import javax.persistence.*;

@Entity
@Table(name = "marks")
public class MarkEntity {
    @Id
    @GeneratedValue
    private Integer id;
    private String mark;

    @ManyToOne(cascade = {CascadeType.ALL})
    private StudentEntity student;

    @ManyToOne(cascade = {CascadeType.ALL})
    private SubjectEntity subject;

    public MarkEntity() {
    }

    public MarkEntity(String mark) {
        this.mark = mark;
    }

    public MarkEntity(Integer id, String mark) {
        this.id = id;
        this.mark = mark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StudentEntity getStudent() {
        return student;
    }

    public void setStudent(StudentEntity student) {
        this.student = student;
    }

    public SubjectEntity getSubject() {
        return subject;
    }

    public void setSubject(SubjectEntity subject) {
        this.subject = subject;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
