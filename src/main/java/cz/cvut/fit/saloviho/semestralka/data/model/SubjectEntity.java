package cz.cvut.fit.saloviho.semestralka.data.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "subjects")
public class SubjectEntity {
    @Id
    @GeneratedValue
    private Integer id;
    private String shortname;
    private String fullname;

    @ManyToMany(mappedBy = "subjects", cascade = {CascadeType.ALL})
    Collection<StudentEntity> appliedStudents;

    @OneToMany(cascade = {CascadeType.ALL})
    Collection<MarkEntity> marks;

    public SubjectEntity() {
    }

    public SubjectEntity(Integer id, String shortname, String fullname) {
        this.id = id;
        this.shortname = shortname;
        this.fullname = fullname;
    }


    public SubjectEntity(String shortname, String fullname) {
        this.shortname = shortname;
        this.fullname = fullname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Collection<StudentEntity> getAppliedStudents() {
        return appliedStudents;
    }

    public void setAppliedStudents(Collection<StudentEntity> appliedStudents) {
        this.appliedStudents = appliedStudents;
    }

    public Collection<MarkEntity> getMarks() {
        return marks;
    }

    public void setMarks(Collection<MarkEntity> marks) {
        this.marks = marks;
    }
}
