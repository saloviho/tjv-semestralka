package cz.cvut.fit.saloviho.semestralka.rest.dto;

import org.springframework.hateoas.RepresentationModel;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "mark")
public class MarkDto extends RepresentationModel<MarkDto> implements Serializable {
    private Integer id;
    private String mark;

    public MarkDto() {
    }

    public MarkDto(Integer id, String mark) {
        this.id = id;
        this.mark = mark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
