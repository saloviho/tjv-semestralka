package cz.cvut.fit.saloviho.semestralka.services;

import cz.cvut.fit.saloviho.semestralka.data.dao.MarkRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.StudentRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.SubjectRepository;
import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private MarkRepository markRepository;

    @Transactional
    public StudentEntity createOrUpdate(StudentEntity e) {
        return studentRepository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<StudentEntity> readAll()
    {
        return studentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<StudentEntity> readById(Integer id) {
        return studentRepository.findById(id);
    }

    @Transactional
    @Cascade(CascadeType.DELETE)
    public void deleteById(Integer id) {
        studentRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Collection<SubjectEntity> readSubjects(String firstName, String lastName) {
        return studentRepository.findAllSubjectsByFirstnameAndLastname(firstName, lastName);
    }

    @Transactional
    public void addSubject(Integer id, Integer subject_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(subject_id);
        if (optionalStudent.isPresent() && optionalSubject.isPresent()) {
            StudentEntity student = optionalStudent.get();
            SubjectEntity subject = optionalSubject.get();
            student.getSubjects().add(subject);
            subject.getAppliedStudents().add(student);
            subjectRepository.save(subject);
            studentRepository.save(student);
        }
    }

    @Transactional
    public void removeSubject(Integer id, Integer subject_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(subject_id);
        if (optionalStudent.isPresent() && optionalSubject.isPresent()) {
            StudentEntity student = optionalStudent.get();
            SubjectEntity subject = optionalSubject.get();
            student.getSubjects().remove(subject);
            subject.getAppliedStudents().remove(student);
            subjectRepository.save(subject);
            studentRepository.save(student);
        }
    }

    @Transactional
    public void addMark(Integer id, Integer mark_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(id);
        Optional<MarkEntity> optionalMark = markRepository.findById(mark_id);
        if (optionalStudent.isPresent() && optionalMark.isPresent()) {
            MarkEntity mark = optionalMark.get();
            StudentEntity student = optionalStudent.get();
            student.getMarks().add(mark);
            mark.setStudent(student);
            markRepository.save(mark);
            studentRepository.save(student);
        }
    }

    @Transactional
    public void removeMark(Integer id, Integer mark_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(id);
        Optional<MarkEntity> optionalMark = markRepository.findById(mark_id);
        if (optionalStudent.isPresent() && optionalMark.isPresent()) {
            StudentEntity student = optionalStudent.get();
            MarkEntity mark = optionalMark.get();
            student.getMarks().remove(mark);
            mark.setStudent(null);
            markRepository.save(mark);
            studentRepository.save(student);
        }
    }
}
