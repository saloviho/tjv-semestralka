package cz.cvut.fit.saloviho.semestralka.rest;

import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import cz.cvut.fit.saloviho.semestralka.rest.dto.MarkDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.StudentDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.SubjectDto;
import cz.cvut.fit.saloviho.semestralka.services.Converter;
import cz.cvut.fit.saloviho.semestralka.services.MarkService;
import cz.cvut.fit.saloviho.semestralka.services.StudentService;
import cz.cvut.fit.saloviho.semestralka.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(StudentDto.class)
@RequestMapping(value = "/api/students")
public class StudentController {
    @Autowired
    StudentService studentService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    MarkService markService;

    @Autowired
    private EntityLinks entityLinks;

    @GetMapping
    public HttpEntity<Collection<StudentDto>> getAll()
    {
        Link linkToList = entityLinks.linkToCollectionResource(StudentDto.class).withRel("list");
        Collection<StudentDto> studentsDto = Converter.STUDENT_TO_DTO.toCollectionModel(studentService.readAll()).getContent();
        return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(studentsDto);
    }

    @PostMapping
    public HttpEntity<StudentDto> create(@RequestBody StudentDto dto)
    {
        StudentEntity e = studentService.createOrUpdate(new StudentEntity(dto.getFirstname(), dto.getLastname()));
        StudentDto result = Converter.STUDENT_TO_DTO.toModel(e);
        return ResponseEntity.created(entityLinks.linkForItemResource(StudentDto.class, result.getId()).toUri()).body(result);
    }

    @PutMapping
    public HttpEntity<StudentDto> updateOrCreate(@RequestBody StudentDto dto)
    {
        Link linkToList = entityLinks.linkToCollectionResource(StudentDto.class).withRel("list");
        StudentEntity e = studentService.createOrUpdate(Converter.DTO_TO_STUDENT(dto));
        StudentDto result = Converter.STUDENT_TO_DTO.toModel(e);
        return ResponseEntity.ok().header(HttpHeaders.LINK, result.getLink("self").toString())
                .header(HttpHeaders.LINK, linkToList.toString()).body(result);
    }


    @GetMapping("/{id}")
    public HttpEntity<StudentDto> getById(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(StudentDto.class).withRel("list");
        Optional<StudentEntity> res = studentService.readById(id);
        if(res.isPresent()) {
            StudentDto dto = Converter.STUDENT_TO_DTO.toModel(res.get());
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString())
                    .header(HttpHeaders.LINK, dto.getLink("self").toString()).body(dto);
        }
        else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(StudentDto.class).withRel("list");
        Optional<StudentEntity> res = studentService.readById(id);
        if(res.isPresent()) {
            studentService.deleteById(res.get().getId());
            return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
        }
        else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @GetMapping("/{id}/subjects")
    public HttpEntity<Collection<SubjectDto>> getSubjects(@PathVariable Integer id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getSubjects(id)).withRel("list");
        Optional<StudentEntity> student = studentService.readById(id);
        if(student.isPresent()) {
            Collection<SubjectEntity> subjects = student.get().getSubjects();
            Collection<SubjectDto> subjectsDto = Converter.SUBJECT_TO_DTO.toCollectionModel(subjects).getContent();
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(subjectsDto);
        }else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @PutMapping("/{id}/subjects/{subject_id}")
    public HttpEntity addSubject(@PathVariable Integer id, @PathVariable Integer subject_id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getSubjects(id)).withRel("list");
        studentService.addSubject(id, subject_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}/subjects/{subject_id}")
    public HttpEntity removeSubject(@PathVariable Integer id, @PathVariable Integer subject_id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getSubjects(id)).withRel("list");
        studentService.removeSubject(id, subject_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @GetMapping("/{id}/marks")
    public HttpEntity<Collection<MarkDto>> getMarks(@PathVariable Integer id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getMarks(id)).withRel("list");
        Optional<StudentEntity> student = studentService.readById(id);
        if(student.isPresent())
        {
            Collection<MarkEntity> marks = student.get().getMarks();
            Collection<MarkDto> marksDto = Converter.MARK_TO_DTO.toCollectionModel(marks).getContent();
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(marksDto);
        }else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @PutMapping("/{id}/marks/{mark_id}")
    public HttpEntity addMark(@PathVariable Integer id, @PathVariable Integer mark_id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getMarks(id)).withRel("list");
        studentService.addMark(id, mark_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}/marks/{mark_id}")
    public HttpEntity removeMark(@PathVariable Integer id, @PathVariable Integer mark_id)
    {
        Link linkToList = linkTo(methodOn(StudentController.class).getMarks(id)).withRel("list");
        studentService.removeMark(id, mark_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

}
