package cz.cvut.fit.saloviho.semestralka.services;

import cz.cvut.fit.saloviho.semestralka.data.dao.MarkRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.StudentRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.SubjectRepository;
import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class SubjectService {
    @Autowired
    private MarkRepository markRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Transactional
    public SubjectEntity createOrUpdate(SubjectEntity e) {
        return subjectRepository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<SubjectEntity> readAll()
    {
        return subjectRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<SubjectEntity> readByName(String shortName) {
        return subjectRepository.findByShortname(shortName);
    }

    @Transactional(readOnly = true)
    public Optional<SubjectEntity> readById(Integer id) {
        return subjectRepository.findById(id);
    }

    @Transactional
    @Cascade(CascadeType.DELETE)
    public void deleteById(Integer id) {
        subjectRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Collection<StudentEntity> readStudents(String shortName) {
        return subjectRepository.findAllAppliedStudentsByShortname(shortName);
    }

    @Transactional
    public void addStudent(Integer id, Integer student_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(student_id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(id);
        if (optionalStudent.isPresent() && optionalSubject.isPresent()) {
            StudentEntity student = optionalStudent.get();
            SubjectEntity subject = optionalSubject.get();
            subject.getAppliedStudents().add(student);
            student.getSubjects().add(subject);
            studentRepository.save(student);
            subjectRepository.save(subject);
        }
    }

    @Transactional
    public void removeStudent(Integer id, Integer student_id) {
        Optional<StudentEntity> optionalStudent = studentRepository.findById(student_id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(id);
        if (optionalStudent.isPresent() && optionalSubject.isPresent()) {
            StudentEntity student = optionalStudent.get();
            SubjectEntity subject = optionalSubject.get();
            subject.getAppliedStudents().remove(student);
            student.getSubjects().remove(subject);
            studentRepository.save(student);
            subjectRepository.save(subject);
        }
    }

    @Transactional
    public void addMark(Integer id, Integer mark_id) {
        Optional<MarkEntity> optionalMark = markRepository.findById(mark_id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(id);
        if (optionalMark.isPresent() && optionalSubject.isPresent()) {
            MarkEntity mark = optionalMark.get();
            if(mark.getSubject() == null)
            {
                SubjectEntity subject = optionalSubject.get();
                subject.getMarks().add(mark);
                mark.setSubject(subject);
                markRepository.save(mark);
                subjectRepository.save(subject);
            }
        }
    }

    @Transactional
    public void removeMark(Integer id, Integer mark_id) {
        Optional<MarkEntity> optionalMark = markRepository.findById(mark_id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(id);
        if (optionalMark.isPresent() && optionalSubject.isPresent()) {
            MarkEntity mark = optionalMark.get();
            SubjectEntity subject = optionalSubject.get();
            subject.getMarks().remove(mark);
            mark.setSubject(null);
            markRepository.save(mark);
            subjectRepository.save(subject);
        }
    }
}
