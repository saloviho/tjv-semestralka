package cz.cvut.fit.saloviho.semestralka.rest;

import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import cz.cvut.fit.saloviho.semestralka.rest.dto.MarkDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.StudentDto;
import cz.cvut.fit.saloviho.semestralka.rest.dto.SubjectDto;
import cz.cvut.fit.saloviho.semestralka.services.Converter;
import cz.cvut.fit.saloviho.semestralka.services.MarkService;
import cz.cvut.fit.saloviho.semestralka.services.StudentService;
import cz.cvut.fit.saloviho.semestralka.services.SubjectService;
import org.hibernate.cache.spi.support.CollectionNonStrictReadWriteAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(MarkDto.class)
@RequestMapping(value = "/api/marks")
public class MarkController {
    @Autowired
    StudentService studentService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    MarkService markService;

    @Autowired
    private EntityLinks entityLinks;

    @GetMapping
    public HttpEntity<Collection<MarkDto>> getAll()
    {
        Link linkToList = entityLinks.linkToCollectionResource(MarkDto.class).withRel("list");
        Collection<MarkDto> marksDto = Converter.MARK_TO_DTO.toCollectionModel(markService.readAll()).getContent();
        return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString()).body(marksDto);
    }

    @PostMapping
    public HttpEntity<MarkDto> create(@RequestBody MarkDto dto)
    {
        MarkEntity e = markService.createOrUpdate(new MarkEntity(dto.getMark()));
        MarkDto result = Converter.MARK_TO_DTO.toModel(e);
        return ResponseEntity.created(entityLinks.linkForItemResource(MarkDto.class, result.getId()).toUri()).body(result);
    }

    @PutMapping
    public HttpEntity<MarkDto> updateOrCreate(@RequestBody MarkDto dto)
    {
        Link linkToList = entityLinks.linkToCollectionResource(MarkDto.class).withRel("list");
        MarkEntity e = markService.createOrUpdate(Converter.DTO_TO_MARK(dto));
        MarkDto result = Converter.MARK_TO_DTO.toModel(e);
        return ResponseEntity.ok().header(HttpHeaders.LINK, result.getLink("self").toString())
            .header(HttpHeaders.LINK, linkToList.toString()).body(result);
    }


    @GetMapping("/{id}")
    public HttpEntity<MarkDto> getById(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(MarkDto.class).withRel("list");
        Optional<MarkEntity> res = markService.readById(id);
        if(res.isPresent()) {
            MarkDto dto = Converter.MARK_TO_DTO.toModel(res.get());
            dto.add(linkToList);
            return ResponseEntity.ok().header(HttpHeaders.LINK, linkToList.toString())
                    .header(HttpHeaders.LINK, dto.getLink("self").toString()).body(dto);
        }
        else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable Integer id)
    {
        Link linkToList = entityLinks.linkToCollectionResource(MarkDto.class).withRel("list");
        Optional<MarkEntity> res = markService.readById(id);
        if(res.isPresent()) {
            markService.deleteById(res.get().getId());
            return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
        }
        else return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @GetMapping("/{id}/subject")
    public HttpEntity<SubjectDto> getSubject(@PathVariable Integer id)
    {
        Optional<MarkEntity> mark = markService.readById(id);
        if(mark.isPresent())
        {
            SubjectEntity subject = mark.get().getSubject();
            SubjectDto dto = Converter.SUBJECT_TO_DTO.toModel(subject);
            return ResponseEntity.ok().header(HttpHeaders.LINK, dto.getLink("self").toString()).body(dto);
        }else return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/subject/{subject_id}")
    public HttpEntity setSubject(@PathVariable Integer id, @PathVariable Integer subject_id)
    {
        Link linkToEl = linkTo(methodOn(MarkController.class).getSubject(id)).withRel("subject");
        markService.setSubject(id, subject_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToEl.toString()).build();
    }

    @GetMapping("/{id}/student")
    public HttpEntity<StudentDto> getStudent(@PathVariable Integer id)
    {
        Optional<MarkEntity> mark = markService.readById(id);
        if(mark.isPresent())
        {
            StudentEntity student = mark.get().getStudent();
            StudentDto dto = Converter.STUDENT_TO_DTO.toModel(student);
            return ResponseEntity.ok().header(HttpHeaders.LINK, dto.getLink("self").toString()).body(dto);
        }else return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/student/{student_id}")
    public HttpEntity addMark(@PathVariable Integer id, @PathVariable Integer student_id)
    {
        Link linkToEl = linkTo(methodOn(MarkController.class).getStudent(id)).withRel("student");
        markService.setStudent(id, student_id);
        return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToEl.toString()).build();
    }
}
