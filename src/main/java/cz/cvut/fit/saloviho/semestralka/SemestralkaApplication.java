package cz.cvut.fit.saloviho.semestralka;

import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.services.StudentService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SemestralkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SemestralkaApplication.class, args);
    }

}
