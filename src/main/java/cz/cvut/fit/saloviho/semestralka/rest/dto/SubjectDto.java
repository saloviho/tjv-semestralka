package cz.cvut.fit.saloviho.semestralka.rest.dto;
import org.springframework.hateoas.RepresentationModel;

import javax.security.auth.Subject;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "subject")
public class SubjectDto extends RepresentationModel<SubjectDto> implements Serializable {
    private Integer id;
    private String shortname, fullname;

    public SubjectDto() {
    }

    public SubjectDto(Integer id, String shortname, String fullname) {
        this.id = id;
        this.shortname = shortname;
        this.fullname = fullname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


}
