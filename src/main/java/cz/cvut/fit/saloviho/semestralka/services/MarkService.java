package cz.cvut.fit.saloviho.semestralka.services;

import cz.cvut.fit.saloviho.semestralka.data.dao.MarkRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.StudentRepository;
import cz.cvut.fit.saloviho.semestralka.data.dao.SubjectRepository;
import cz.cvut.fit.saloviho.semestralka.data.model.MarkEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.StudentEntity;
import cz.cvut.fit.saloviho.semestralka.data.model.SubjectEntity;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class MarkService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private MarkRepository markRepository;

    @Transactional
    public MarkEntity createOrUpdate(MarkEntity e) {
        return markRepository.save(e);
    }

    @Transactional(readOnly = true)
    public Optional<MarkEntity> readById(Integer id) {
        return markRepository.findById(id);
    }

    @Transactional
    @Cascade(CascadeType.DELETE)
    public void deleteById(Integer id) {
        markRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Collection<MarkEntity> readAll()
    {
        return markRepository.findAll();
    }

    @Transactional
    public void setSubject(Integer id, Integer subject_id)
    {
        Optional<MarkEntity> optionalMark = markRepository.findById(id);
        Optional<SubjectEntity> optionalSubject = subjectRepository.findById(subject_id);
        if(optionalMark.isPresent() && optionalSubject.isPresent())
        {
            MarkEntity mark = optionalMark.get();
            SubjectEntity subject = optionalSubject.get();
            mark.setSubject(subject);
            subject.getMarks().add(mark);
            subjectRepository.save(subject);
            markRepository.save(mark);
        }
    }

    @Transactional
    public void setStudent(Integer id, Integer student_id)
    {
        Optional<MarkEntity> optionalMark = markRepository.findById(id);
        Optional<StudentEntity> optionalStudent = studentRepository.findById(student_id);
        if(optionalMark.isPresent() && optionalStudent.isPresent())
        {
            MarkEntity mark = optionalMark.get();
            StudentEntity student = optionalStudent.get();
            mark.setStudent(student);
            student.getMarks().add(mark);
            studentRepository.save(student);
            markRepository.save(mark);
        }
    }
}
